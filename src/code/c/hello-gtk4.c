/*
 * Hello world with GTK4 in C
 *
 * By Mohammed Sadiq <sadiq [at] sadiqpk [d0t] org>
 * Date: 2020 May 10
 * License: None, Public domain (CC0)
 *
 * Compile:
 *   gcc hello.c $(pkg-config --cflags --libs gtk4) -o hello
 *
 * Run:
 *   ./hello
 */

#include <gtk/gtk.h>

static void
activate_cb (GtkApplication *app,
             gpointer        user_data)
{
  GtkWindow *window;
  GtkWidget *label;

  window = GTK_WINDOW (gtk_application_window_new (app));
  gtk_window_set_title (window, "Hello World!");
  gtk_window_set_default_size (window, 400, 300);

  label = gtk_label_new ("Hello World!");
  gtk_window_set_child (window, label);

  gtk_window_present (window);
}

int
main (int    argc,
      char **argv)
{
  g_autoptr(GtkApplication) app = NULL;

  app = gtk_application_new ("org.sadiqpk.example", 0);
  g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);

  return g_application_run (G_APPLICATION (app), argc, argv);
}
